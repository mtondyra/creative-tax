"""
Creates Creative Tax Report as Excel sheet from git commits and JIRA issues per one month periods.
JIRA issues keys are extracted from commit messages e.g.: feat: JR-1234 commit-msg

Usage:
 1. install python >= 3.7
 2. install dependencies:
    > pip install openpyxl
    > pip install GitPython
 3. create Github and JIRA security tokens
 4. fill in globals:
    - PROJECTS_DIR
    - JIRA_USER
    - JIRA_TOKEN
    - LEGEND
"""
from git import Repo
import datetime as dt
import calendar
import re
import requests
import enum
import glob
from collections import namedtuple, defaultdict

from openpyxl import Workbook
from openpyxl.styles import Alignment, Font
from openpyxl.utils import get_column_letter

from github import Github

from typing import List


# Path to repositories to be scanned
PROJECTS_DIR = "/Users/mlachowicz/Projects"


JIRA_URL = "https://oandacorp.atlassian.net/rest/api/3"
JIRA_USER = "mtondyra@oanda.com"
JIRA_TOKEN = "axbKmfRfFxxxxxxxxxxxxxxxxxxx"

JIRA_ISSUE_PATTERN = r"\w+-\d+"

Commit = namedtuple("Commit", "repo sha url date msg")
Issue = namedtuple("Issue", "key summary")

UNSPECIFIED_ISSUE = Issue("unspecified", "unspecified")

LEGEND = (("Employee", "Michal Tondyra"), ("Employee Id", "80377"), ("Job position", "Senior Quant Developer"))

LEGEND_FONT = Font(name="Consolas", size=11, bold=True)
LEGEND_ALIGNMENT = Alignment(horizontal='left', vertical='center', wrap_text=False)
LEGEND_REPORTING_PERIOD = "Reporting period"
LEGEND_SUBMISSION_DATE = "Submission date"
LEGEND_STAR = "[*]Eng: Is it a work created as part of creative activity according to Article 22 para. 9b PIT?"
LEGEND_STAR_PL = "[*]Pl: Czy praca stanowi utwór wytworzony w ramach działalności twórczej z art. 22 ust. 9b PIT?"

HEADER = ["JIRA issue ticket", "JIRA issue summary", "Commit id", "Commit repo", "Commit date", "Commit message",
          "Creative work [*]", "Type of work"]
HEADER_FONT = Font(name="Consolas", size=10, bold=True)
HEADER_ALIGNMENT = Alignment(horizontal="center", vertical="center", wrap_text=False)

BODY_FONT = Font(name="Consolas", size=9, bold=False)
BODY_ALIGNMENT_LEFT = Alignment(horizontal='left', vertical='center', wrap_text=True)
BODY_ALIGNMENT_CENTERED = Alignment(horizontal='center', vertical='center', wrap_text=True)

LINK_FONT = Font(name="Consolas", size=7, bold=False, italic=True, color="0000ff")

COMMIT_MSG_COLUMN_WIDTH = 50
ISSUE_SUMMARY_COLUMN_WIDTH = 50
COMMIT_SHA_COLUMN_WIDTH = 15.5  # magic number, so sha fits in 2 lines

START_AT_COLUMN = 2

CONSOLAS_WIDTH_FACTOR = 1.2


class TypeOfWork(enum.Enum):
    SRC_CODE = "src code/kod źródłowy"
    DOC = "doc/dokumentacja"


class CreativerWork(enum.Enum):
    YES = "yes/tak"
    NO = "no/nie"


def get_commits(year, month):
	"""
	Returns list of all commits in specified month.
	"""
	result = []
	commits_sha = set()  # to remove duplicate commits reported in branches
	since = dt.datetime(year, month, day=1, hour=0,
						minute=0, second=0, microsecond=0)
	last_month_day = calendar.monthrange(year, month)[1]
	until = dt.datetime(year, month, last_month_day, hour=23,
						minute=59, second=59, microsecond=999999)

	for dir in glob.glob(f"{PROJECTS_DIR}/*/"):
		r = Repo(dir)
		repo_name = r.remotes.origin.url.split('.git')[0].split('/')[-1]
		reader = r.config_reader()
		current_user = reader.get_value("user", "email")
		
		commits = r.iter_commits('--all', author=current_user, after=since.strftime(
			"{%Y-%m-%d}"), before=until.strftime("{%Y-%m-%d}"))
		for commit in commits:
			sha = r.git.rev_parse(commit.hexsha)
			if sha not in commits_sha:
				result.append(Commit(f"{repo_name}", sha, f"https://bitbucket.org/oandacorp/{repo_name}/commits/{sha}",
									dt.datetime.fromtimestamp(commit.committed_date).strftime('%Y-%m-%d'), commit.message))
				commits_sha.add(sha)
	return result


def get_issue_summary(key):
    """
    Returns JIRA issue summary by its key or None if there is no such issue.
    """
    with requests.get(f"{JIRA_URL}/issue/{key}", auth=(JIRA_USER, JIRA_TOKEN)) as r:
        if r.status_code == requests.codes.ok:
            return r.json()["fields"]["summary"]


def cluster_by_jira_issues(commits):
	"""
	Clusters commits by JIRA issues specified in commits messages.
	"""
	result = defaultdict(list)
	issues = {}
	re_exp = re.compile(JIRA_ISSUE_PATTERN)
	for commit in commits:
		found = re.findall(re_exp, commit.msg)
		if not found:
			result[UNSPECIFIED_ISSUE].append(commit)
			continue
		for key in set(found):
			if key in issues:
				result[issues[key]].append(commit)
				continue
			summary = get_issue_summary(key)
			if summary is None:
				result[UNSPECIFIED_ISSUE].append(commit)
				continue
			issues[key] = Issue(key, summary)
			result[issues[key]].append(commit)
	return result


def set_cell(cell, value, font, alignment):
    cell.value = value
    cell.font = font
    cell.alignment = alignment


def fill_legend(sheet, starting_row, reporting_period, submission_date):
    """Returns number of next, not affected row"""
    legend = list(LEGEND)
    legend.append((LEGEND_REPORTING_PERIOD, reporting_period))
    legend.append((LEGEND_SUBMISSION_DATE, submission_date))

    col1_width = max([len(i[0]) for i in legend])
    sheet.column_dimensions[get_column_letter(1)].width = col1_width * CONSOLAS_WIDTH_FACTOR

    row = starting_row

    for label, value in legend:
        set_cell(sheet.cell(row, 1), label, LEGEND_FONT, LEGEND_ALIGNMENT)
        set_cell(sheet.cell(row, 2), value, LEGEND_FONT, LEGEND_ALIGNMENT)
        row += 1

    set_cell(sheet.cell(row, 1), LEGEND_STAR, LEGEND_FONT, LEGEND_ALIGNMENT)
    row += 1
    set_cell(sheet.cell(row, 1), LEGEND_STAR_PL, LEGEND_FONT, LEGEND_ALIGNMENT)
    row += 1

    return row


def fill_header(sheet, row, commits):
    column = START_AT_COLUMN

    for i in range(len(HEADER)):
        set_cell(sheet.cell(row, column + i), HEADER[i], HEADER_FONT, HEADER_ALIGNMENT)
        sheet.column_dimensions[get_column_letter(column + i)].width = len(HEADER[i]) * CONSOLAS_WIDTH_FACTOR

    sheet.column_dimensions[get_column_letter(column + 1)].width = ISSUE_SUMMARY_COLUMN_WIDTH * CONSOLAS_WIDTH_FACTOR

    max_repo_len = max([len(c1.repo) for c2 in commits.values() for c1 in c2])
    sheet.column_dimensions[get_column_letter(column + 3)].width = max_repo_len * CONSOLAS_WIDTH_FACTOR

    sheet.column_dimensions[get_column_letter(column + 2)].width = COMMIT_SHA_COLUMN_WIDTH

    max_date_len = max([len(str(c1.date)) for c2 in commits.values() for c1 in c2])
    sheet.column_dimensions[get_column_letter(column + 4)].width = max_date_len * CONSOLAS_WIDTH_FACTOR

    sheet.column_dimensions[get_column_letter(column + 5)].width = COMMIT_MSG_COLUMN_WIDTH * CONSOLAS_WIDTH_FACTOR

    sheet.column_dimensions[get_column_letter(column + 7)].width = \
        max([len(i.value) for i in TypeOfWork]) * CONSOLAS_WIDTH_FACTOR


def fill_issue(sheet, row, issue):
    column = START_AT_COLUMN

    set_cell(sheet.cell(row, column), issue.key, BODY_FONT, BODY_ALIGNMENT_LEFT)
    set_cell(sheet.cell(row, column + 1), issue.summary, BODY_FONT, BODY_ALIGNMENT_LEFT)


def fill_commit(sheet, row, commit):
    column = START_AT_COLUMN

    cell = sheet.cell(row, column + 2)
    set_cell(cell, commit.sha, LINK_FONT, BODY_ALIGNMENT_LEFT)
    cell.hyperlink = commit.url

    set_cell(sheet.cell(row, column + 3), commit.repo, BODY_FONT, BODY_ALIGNMENT_CENTERED)
    set_cell(sheet.cell(row, column + 4), str(commit.date), BODY_FONT, BODY_ALIGNMENT_CENTERED)
    set_cell(sheet.cell(row, column + 5), commit.msg, BODY_FONT, BODY_ALIGNMENT_LEFT)
    set_cell(sheet.cell(row, column + 6), CreativerWork.YES.value, BODY_FONT, BODY_ALIGNMENT_CENTERED)
    set_cell(sheet.cell(row, column + 7), TypeOfWork.SRC_CODE.value, BODY_FONT, BODY_ALIGNMENT_CENTERED)


def create_crtax_report(year, month):
    commits = cluster_by_jira_issues(get_commits(year, month))

    wb = Workbook()
    sheet = wb.active
    sheet.title = f"{month:02}-{year}"

    row = fill_legend(sheet, starting_row=2, reporting_period=f"{month:02}-{year}",
                     submission_date=str(dt.datetime.utcnow().date()))

    fill_header(sheet, row + 2, commits)

    row += 3

    for issue in commits:
        fill_issue(sheet, row, issue)
        row += 1
        commits[issue].sort(key=lambda i: i.date)
        for commit in commits[issue]:
            fill_commit(sheet, row, commit)
            row += 1

    file_name = f"Creative_Tax_Report_{LEGEND[0][1].replace(' ', '_')}_{month:02}-{year}.xlsx"
    wb.save(file_name)
    print(f"Saved report in file: {file_name}")


if __name__ == "__main__":
    now = dt.datetime.now()
    create_crtax_report(year=now.year, month=now.month)

